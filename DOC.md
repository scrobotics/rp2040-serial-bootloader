# Bootloader

## Install bootloader

```shell
git clone git@gitlab.com:scrobotics/rp2040-serial-bootloader.git
```

## Compile bootloader

```shell
cd rp2040-serial-bootloader/
export PICO_SDK_PATH=~/projects/tools/pico-sdk/
cmake CMakeLists.txt
make
```

## Concatenate complete file

```shell
dd if=rp2040-serial-bootloader/bootloader.bin of=mixed.bin bs=1 conv=notrunc
python3 rp2040-serial-bootloader/gen_imghdr.py mesh_gauge_rp2040/.pio/build/pico/firmware.bin header.bin
dd if=header.bin of=mixed.bin bs=1 seek=12288  conv=notrunc
dd if=mesh_gauge_rp2040/.pio/build/pico/firmware.bin of=mixed.bin bs=1 seek=16384  conv=notrunc
```

## Flash binary firmware

```shell
openocd  -f interface/cmsis-dap.cfg -f target/rp2040.cfg -c "adapter speed 5000" -c "program mixed.bin 0x10000000 reset exit"
```
