#include <stdio.h>
#include <string.h>

static void uint32_to_string(const uint32_t val, char* buffer) {
    // create a string representation of val in hex format with leading zeros
    // buffer must be at least 9 bytes long
    // without snprintf
    const char hex[] = "0123456789abcdef";
    buffer[0] = '0';
    buffer[1] = 'x';
    buffer[2] = hex[(val >> 28) & 0xf];
    buffer[3] = hex[(val >> 24) & 0xf];
    buffer[4] = hex[(val >> 20) & 0xf];
    buffer[5] = hex[(val >> 16) & 0xf];
    buffer[6] = hex[(val >> 12) & 0xf];
    buffer[7] = hex[(val >> 8) & 0xf];
    buffer[8] = hex[(val >> 4) & 0xf];
    buffer[9] = hex[val & 0xf];
    buffer[10] = 0;
}

static void uint16_to_string(const uint16_t val, char* buffer) {
    // create a string representation of val in hex format with leading zeros
    // buffer must be at least 5 bytes long
    // without snprintf
    const char hex[] = "0123456789abcdef";
    buffer[0] = hex[(val >> 12) & 0xf];
    buffer[1] = hex[(val >> 8) & 0xf];
    buffer[2] = hex[(val >> 4) & 0xf];
    buffer[3] = hex[val & 0xf];
    buffer[4] = 0;
}

static void uint8_to_string(const uint8_t val, char* buffer) {
    // create a string representation of val in hex format with leading zeros
    // buffer must be at least 3 bytes long
    // without snprintf
    const char hex[] = "0123456789abcdef";
    buffer[0] = hex[(val >> 4) & 0xf];
    buffer[1] = hex[val & 0xf];
    buffer[2] = 0;
}

static void fill_buffer_with_uint32_t(uint8_t* buffer, const uint32_t val, const uint8_t first_pos) {
    buffer[first_pos] = val & 0xff;
    buffer[first_pos + 1] = (val >> 8) & 0xff;
    buffer[first_pos + 2] = (val >> 16) & 0xff;
    buffer[first_pos + 3] = (val >> 24) & 0xff;
}

static void uint64_to_string(const uint64_t val, char* buffer) {
    // create a string representation of val in hex format with leading zeros
    // buffer must be at least 17 bytes long
    // without snprintf
    const char hex[] = "0123456789abcdef";
    buffer[0] = '0';
    buffer[1] = 'x';
    buffer[2] = hex[(val >> 60) & 0xf];
    buffer[3] = hex[(val >> 56) & 0xf];
    buffer[4] = hex[(val >> 52) & 0xf];
    buffer[5] = hex[(val >> 48) & 0xf];
    buffer[6] = hex[(val >> 44) & 0xf];
    buffer[7] = hex[(val >> 40) & 0xf];
    buffer[8] = hex[(val >> 36) & 0xf];
    buffer[9] = hex[(val >> 32) & 0xf];
    buffer[10] = hex[(val >> 28) & 0xf];
    buffer[11] = hex[(val >> 24) & 0xf];
    buffer[12] = hex[(val >> 20) & 0xf];
    buffer[13] = hex[(val >> 16) & 0xf];
    buffer[14] = hex[(val >> 12) & 0xf];
    buffer[15] = hex[(val >> 8) & 0xf];
    buffer[16] = hex[(val >> 4) & 0xf];
    buffer[17] = hex[val & 0xf];
    buffer[18] = 0;
}

static void fill_hex_buffer_with_uint8_t_buffer(char* buffer, const uint8_t* val, const uint8_t size) {
    const char hex[] = "0123456789abcdef";
    for (uint8_t i = 0; i < size; i++) {
        buffer[i * 2] = hex[(val[i] >> 4) & 0x0F];
        buffer[i * 2 + 1] = hex[val[i] & 0x0F];
    }
    buffer[size * 2 + 2] = 0;
}