/**
 * Copyright (c) 2021 Brian Starkey <stark3y@gmail.com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <stdio.h>
#include <string.h>

#include "RP2040.h"
#include "hardware/dma.h"
#include "hardware/flash.h"
#include "hardware/gpio.h"
#include "hardware/resets.h"
#include "hardware/structs/dma.h"
#include "hardware/structs/watchdog.h"
#include "hardware/sync.h"
#include "hardware/uart.h"
#include "hardware/watchdog.h"
#include "pico/time.h"

// #include "libsodium-stable/src/libsodium/include/sodium.h"
// #include "libsodium-stable/src/libsodium/include/sodium/crypto_onetimeauth.h"

#include "monocypher-4.0.2/src/optional/monocypher-ed25519.h"

#include "functions.h"

// openssl ec -in private.pem -text -noout
// Take the public result, remove the first byte (04) and create an array of unsigned char with the rest of the bytes
#include "pass.h"

// The bootloader can be entered in two ways:
//  - No valid image header
//  - A button is pressed

#include "defined_data.h"

// #define PRINT

const uint32_t IMAGE_HEADER_ADDRESS = XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET;
const uint32_t FW_ADDRESS = XIP_BASE + FW_RUN_OFFSET;

static void disable_interrupts(void) {
    SysTick->CTRL &= ~1;

    NVIC->ICER[0] = 0xFFFFFFFF;
    NVIC->ICPR[0] = 0xFFFFFFFF;
}

static void reset_peripherals(void) {
    reset_block(~(RESETS_RESET_IO_QSPI_BITS | RESETS_RESET_PADS_QSPI_BITS | RESETS_RESET_SYSCFG_BITS |
                  RESETS_RESET_PLL_SYS_BITS));
}

static void jump_to_vtor(uint32_t vtor) {
    // Derived from the Leaf Labs Cortex-M3 bootloader.
    // Copyright (c) 2010 LeafLabs LLC.
    // Modified 2021 Brian Starkey <stark3y@gmail.com>
    // Originally under The MIT License
    uint32_t reset_vector = *(volatile uint32_t*)(vtor + 0x04);

    SCB->VTOR = (volatile uint32_t)(vtor);

    asm volatile("msr msp, %0" ::"g"(*(volatile uint32_t*)vtor));
    asm volatile("bx %0" ::"r"(reset_vector));
}

static bool image_header_ok(struct ImageHeader_t* hdr) {
    const uint32_t fw_addr = XIP_BASE + FW_RUN_OFFSET;
    uint32_t* vtor = (uint32_t*)hdr->vtor;

    // Stack pointer needs to be in RAM
    if (vtor[0] < SRAM_BASE) {
#ifdef PRINT
        uart_puts(uart1, "SP OUT OF RANGE\r\n");
#endif
        return false;
    }

    // Reset vector should be in the image, and thumb (bit 0 set)
    // if ((vtor[1] < hdr->vtor) || (vtor[1] > hdr->vtor + hdr->size) || !(vtor[1] & 1)) {
    if ((vtor[1] < fw_addr) || (vtor[1] > fw_addr + hdr->size) || !(vtor[1] & 1)) {
#ifdef PRINT
        uart_puts(uart1, "RESET VECTOR OUT OF RANGE\r\n");
#endif
        return false;
    }

    // Looks OK.
    return true;
}

static void do_reboot(bool to_bootloader) {
    hw_clear_bits(&watchdog_hw->ctrl, WATCHDOG_CTRL_ENABLE_BITS);
    watchdog_reboot(0, 0, 0);
    while (1) {
        tight_loop_contents();
        asm("");
    }
}

static void setup_pin(const uint8_t pin, const bool is_output, const uint8_t state) {
    gpio_init(pin);
    gpio_set_dir(pin, is_output);
    if (is_output)
        gpio_put(pin, state);
}

static uint32_t get_offset_if_any_sw_is_pressed() {
    for (int i = 0; i < INPUT_PINS_SIZE; i++) {
        if (gpio_get(INPUT_PINS[i]) == 0) {
            return IMAGE_HEADER_OFFSETS[i];
        }
    }
    return SELECTED_IMAGE_HEADER_OFFSET;
}

static void setup_pins() {
    setup_pin(POWER_PIN, true, 1);
    setup_pin(MOTOR_ENABLE_PIN, true, 1);
    setup_pin(LED_PIN, true, 1);

    setup_pin(SW_UP_PIN, false, 0);
    setup_pin(SW_OK_PIN, false, 0);
    setup_pin(SW_DOWN_PIN, false, 0);
}

static void setup_uart() {
#ifdef PRINT
    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);
    uart_init(uart1, UART_BAUD);
    uart_set_hw_flow(uart1, false, false);
    uart_puts(uart1, "SETUP\r\n");
    sleep_ms(100);
#endif
}

static void goto_fw(const uint32_t vtor) {
    disable_interrupts();
    reset_peripherals();
    jump_to_vtor(vtor);
}

static bool pull(uint8_t* dest, const uint16_t size, const uint64_t address) {
    uint32_t ints = save_and_disable_interrupts();
    for (uint64_t kx = 0; kx < size; kx++) {
        uint8_t* ptr = (uint8_t*)(address + kx);
        dest[kx] = *ptr;
    }
    restore_interrupts(ints);

    return true;
}

static bool push(const uint8_t* src, const uint16_t size, const uint64_t address) {
    if (size > FLASH_SECTOR_SIZE)
        return false;

    if (size % 256 != 0)
        return false;

    uint32_t ints = save_and_disable_interrupts();
    flash_range_erase(address, FLASH_SECTOR_SIZE);
    flash_range_program(address, src, size);
    restore_interrupts(ints);
    return true;
}

static void update_image_header_selected_info(const uint32_t address_to_update, const ImageHeader_t* hdr) {
#ifdef PRINT
    uart_puts(uart1, "UPDATING IMAGE HEADER\r\n");
#endif
    uint8_t buffer[BUFFER_SIZE] = {0};

    pull(buffer, BUFFER_SIZE, XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET);

    const uint32_t vtor = hdr->vtor;
    const uint32_t size = hdr->size;
    const uint32_t offset = address_to_update - (XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET);

    fill_buffer_with_uint32_t(buffer, vtor, offset);
    fill_buffer_with_uint32_t(buffer, size, offset + 4);
    for (int i = 0; i < SIGN_SIZE; i++) {
        buffer[offset + 8 + i] = hdr->signature[i];
    }
    buffer[offset + 8 + SIGN_SIZE] = 0;

    push(buffer, BUFFER_SIZE, SELECTED_IMAGE_HEADER_OFFSET);
}

static FW get_fw_with_signature(const char signature[SIGN_SIZE]) {
    bool is_equal = true;
    const uint32_t base_offset = XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET;

    for (uint8_t i = 0; i < STORED_FW_NUM; i++) {
        struct ImageHeader_t* hdr = (struct ImageHeader_t*)(base_offset + IMAGE_HEADER_OFFSETS[i]);
        is_equal = true;

        for (uint8_t j = 0; j < SIGN_SIZE; j++) {
            if (hdr->signature[j] != signature[j]) {
                is_equal = false;
                break;
            }
        }

        if (is_equal) {
#ifdef PRINT
            char text[32];
            uart_puts(uart1, "FOUND FW -> ");
            uint8_to_string(i, text);
            uart_puts(uart1, text);
            uart_puts(uart1, "\r\n");
#endif
            return (FW)(i);
        }
    }
    return NONE;
}

static void update_fw(const uint32_t address, const uint32_t size) {
#ifdef PRINT
    uart_puts(uart1, "UPDATING FW\r\n");
#endif
    uint8_t buffer[BUFFER_SIZE] = {0};
    uint64_t index = 0;
    while (index < size) {
        pull(buffer, BUFFER_SIZE, address + index);
        push(buffer, BUFFER_SIZE, FW_RUN_OFFSET + index);
        index += BUFFER_SIZE;
    }
}

static void print(const char* name, const ImageHeader_t* hdr) {
#ifdef PRINT
    char text[64];
    uart_puts(uart1, "\r\n--------------------------------");
    uart_puts(uart1, name);
    uart_puts(uart1, "--------------------------------\r\n");
    uart_puts(uart1, "{VTOR: ");
    uint32_to_string(hdr->vtor, text);
    uart_puts(uart1, text);
    uart_puts(uart1, ", SIZE: ");
    uint32_to_string(hdr->size, text);
    uart_puts(uart1, text);
    uart_puts(uart1, ", SIGNATURE: ");
    char text_2[256];
    for (int i = 0; i < sizeof(text_2); i++) {
        text_2[i] = '\0';
    }

    fill_hex_buffer_with_uint8_t_buffer(text_2, (uint8_t*)hdr->signature, SIGN_SIZE);
    uart_puts(uart1, text_2);
    uart_puts(uart1, ", FLAG: ");
    uint8_to_string(hdr->flag_to_update, text);
    uart_puts(uart1, text);
    uart_puts(uart1, "}\r\n");
#endif
};

bool fw_is_signed_correctly(const uint32_t address, const ImageHeader_t* hdr) {
    const uint8_t* ptr_to_vtor = (uint8_t*)(address);
#ifdef PRINT

    uart_puts(uart1, "Address to verify: ");
    char text[32];
    uint32_to_string(address, text);
    uart_puts(uart1, text);
    uart_puts(uart1, "\r\n");

    print("Checking SIGN for FW", hdr);
#endif

    // Call the function with the correct argument types.
    int val = _ed25519_check(hdr->signature, PUBLIC_KEY, ptr_to_vtor, hdr->size);

    // uint8_t generated_signature[SIGN_SIZE];
    // _ed25519_sign(generated_signature, PRIVATE_KEY, ptr_to_vtor, hdr->size);
    // char text_new_signature[256];
    // for (int i = 0; i < sizeof(text_new_signature); i++) {
    // text_new_signature[i] = '\0';
    // }
    // fill_hex_buffer_with_uint8_t_buffer(text_new_signature, generated_signature, SIGN_SIZE);
    // uart_puts(uart1, "Generated SIGNATURE: ");
    // uart_puts(uart1, text_new_signature);
    // uart_puts(uart1, "\r\n");

    if (val < 0) {
#ifdef PRINT
        uart_puts(uart1, "SIGNATURE VERIFY FAILED\r\n");
#endif
        return false;
    }
#ifdef PRINT
    uart_puts(uart1, "SIGNATURE VERIFY OK\r\n");
#endif
    return true;
}

bool check_if_img_headers_are_equals(const ImageHeader_t* current_header, const ImageHeader_t* new_header) {
    const bool equal_vtor = current_header->vtor == new_header->vtor;
    const bool equal_size = current_header->size == new_header->size;
    bool equal_signature = true;
    for (int i = 0; i < SIGN_SIZE; i++) {
        if (current_header->signature[i] != new_header->signature[i]) {
            equal_signature = false;
            break;
        }
    }
    return equal_vtor && equal_size && equal_signature;
}

uint32_t get_image_header_address_to_store_fw(const uint32_t vtor) {
    struct ImageHeader_t* ota_1_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[1]);
    struct ImageHeader_t* ota_2_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[2]);

    if (vtor == ota_2_hdr->vtor) {
        return IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[2];
    }
    return IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[1];
}

bool check_if_bytes_are_equals_for_running_and_selected_fw(const uint32_t vtor, const uint32_t size) {
    const uint32_t fw_addr = XIP_BASE + FW_RUN_OFFSET;
    uint32_t* vtor_selected = (uint32_t*)(vtor + size);
    uint32_t* vtor_running = (uint32_t*)(fw_addr + size);
    for (uint32_t i = 0x100; i < 0; i--) {
        if (vtor_selected[i] != vtor_running[i]) {
            return false;
        }
    }
    return true;
}

void update_hdr_and_fw(const uint32_t img_address, const ImageHeader_t* hdr) {
    update_image_header_selected_info(img_address, hdr);
    update_fw(hdr->vtor, hdr->size);
}

FW get_fw_selected(const ImageHeader_t* hdr) {
    for (int i = 0; i < STORED_FW_NUM; i++) {
        struct ImageHeader_t* offset_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[i]);
        if (check_if_img_headers_are_equals(hdr, offset_hdr)) {
            return (FW)(i);
        }
    }
    return NONE;
}

bool check_if_fw_is_correct(const ImageHeader_t* hdr) {
    if (hdr->vtor == 0 || hdr->size == 0 || hdr->size > MAX_FW_SIZE) {
        return false;
    }
    return fw_is_signed_correctly(hdr->vtor, hdr) && image_header_ok(hdr);
}

void store_default_fw() {
    struct ImageHeader_t* default_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[0]);
    update_image_header_selected_info(XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET, default_hdr);
    update_hdr_and_fw(XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET, default_hdr);
}

void check_new_fw_and_update_it(const ImageHeader_t* hdr, const FW fw_selected) {
    const bool is_fw_correct = check_if_fw_is_correct(hdr);
    if (!is_fw_correct) {
#ifdef PRINT
        uart_puts(uart1, "FW IS NOT CORRECT\r\n");
#endif
    }
    if (!is_fw_correct || fw_selected == DEFAULT || fw_selected == NONE) {
#ifdef PRINT
        uart_puts(uart1, "STORING DEFAULT\r\n");
#endif
        store_default_fw();
        if (fw_is_signed_correctly(hdr->vtor, hdr)) {
#ifdef PRINT
            uart_puts(uart1, "DEFAULT FW IS SIGNED CORRECTLY\r\n");
#endif
        }
        return;
    }

#ifdef PRINT
    uart_puts(uart1, "FW IS CORRECT -> STORING OTA\r\n");
#endif
    const uint32_t img_address = get_image_header_address_to_store_fw(hdr->vtor);
    update_image_header_selected_info(XIP_BASE + SELECTED_IMAGE_HEADER_OFFSET, hdr);
    update_hdr_and_fw(img_address, hdr);
}

uint8_t get_switches_state() {
    uint8_t state = 0;
    for (int i = 0; i < INPUT_PINS_SIZE; i++) {
        if (gpio_get(INPUT_PINS[i]) == 0) {
            state |= (1 << i);
        }
    }
    return state;
}

FW get_fw_if_any_sw_is_pressed(const uint8_t state) {
    switch (state) {
        case 0b00000001:
#ifdef PRINT
            uart_puts(uart1, "SELECTED DEFAULT\r\n");
#endif
            return DEFAULT;
        case 0b00000010:
#ifdef PRINT
            uart_puts(uart1, "SELECTED OTA_1\r\n");
#endif
            return OTA_1;
        case 0b00000100:
#ifdef PRINT
            uart_puts(uart1, "SELECTED OTA_2\r\n");
#endif
            return OTA_2;
        default:
            return NONE;
    }
    return NONE;
}

void print_current_img_headers() {
    struct ImageHeader_t* current_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS);
    struct ImageHeader_t* default_1_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[0]);
    struct ImageHeader_t* ota_1_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[1]);
    struct ImageHeader_t* ota_2_hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[2]);

    print("CURRENT", current_hdr);
    // fw_is_signed_correctly(current_hdr->vtor, current_hdr);
    print("DEFAULT", default_1_hdr);
    // fw_is_signed_correctly(default_1_hdr->vtor, default_1_hdr);
    print("OTA_1", ota_1_hdr);
    // fw_is_signed_correctly(ota_1_hdr->vtor, ota_1_hdr);
    print("OTA_2", ota_2_hdr);
    // fw_is_signed_correctly(ota_2_hdr->vtor, ota_2_hdr);
}

int main(void) {
    setup_pins();
    setup_uart();

#ifdef PRINT
    uart_puts(uart1, "\r\n\r\n========================================= INIT ================================\r\n\r\n");
    print_current_img_headers();
#endif

    struct ImageHeader_t* hdr;
    FW fw_selected = NONE;

    const uint8_t state = get_switches_state();
    if (state != 0) {
        fw_selected = get_fw_if_any_sw_is_pressed(state);
        hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS + IMAGE_HEADER_OFFSETS[fw_selected]);
#ifdef PRINT
        print("SELECTED FW with SW", hdr);
#endif
        check_new_fw_and_update_it(hdr, fw_selected);
    } else {
        hdr = (struct ImageHeader_t*)(IMAGE_HEADER_ADDRESS);
        fw_selected = get_fw_selected(hdr);
#ifdef PRINT
        print("SELECTED FW", hdr);
#endif
        if (hdr->flag_to_update || hdr->vtor == 0 || hdr->size == 0 || hdr->size > MAX_FW_SIZE) {
            check_new_fw_and_update_it(hdr, fw_selected);
        }
    }
#ifdef PRINT
    sleep_ms(100);
    uart_puts(uart1,
              "\r\n\r\n========================================= END   ================================\r\n\r\n");
    print_current_img_headers();
    sleep_ms(100);
#endif

    goto_fw(FW_ADDRESS);

    return 0;
}
